﻿using System.Data.Entity;
using WikeSoft.Core.Exception;

namespace WikeSoft.Enterprise
{
    /// <summary>
    /// dbSet扩展
    /// </summary>
    public static class DbContextExtention
    {
        /// <summary>
        /// 根据主键查找记录，如果未找到，则抛出异常
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="db"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static T Load<T>(this DbSet<T> db, object key) where T : class
        {
            var target = db.Find(key);
            if (target == null)
            {
                throw new TipInfoException("未找到相关记录，key=" + key);
            }
            return target;
        }
    }
}
