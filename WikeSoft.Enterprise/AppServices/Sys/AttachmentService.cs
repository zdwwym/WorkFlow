﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using Mehdime.Entity;
using WikeSoft.Core.Extension;
using WikeSoft.Data;
using WikeSoft.Data.Models;
using WikeSoft.Enterprise.Entities;
using WikeSoft.Enterprise.Interfaces.Sys;
using WikeSoft.Enterprise.Models.Sys;

namespace WikeSoft.Enterprise.AppServices.Sys
{
    /// <summary>
    /// 附件服务
    /// </summary>
    public class AttachmentService : IAttachmentService
    {
        private readonly IMapper _mapper;
        private readonly IDbContextScopeFactory _dbContextScopeFactory;
        public AttachmentService(IMapper mapper, IDbContextScopeFactory dbContextScopeFactory)
        {
            _mapper = mapper;
            _dbContextScopeFactory = dbContextScopeFactory;
        }
        /// <summary>
        /// 添加附件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool Add(AttachmentAddModel model)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var entity = _mapper.Map<AttachmentAddModel, SysAttachment>(model);
                db.SysAttachments.Add(entity);
                return scope.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 获取
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns></returns>
        public AttachmentModel Find(string id)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var entity = db.SysAttachments.Load(id);
                return _mapper.Map<SysAttachment, AttachmentModel>(entity);
            }
        }
        /// <summary>
        /// 添加图片
        /// </summary>
        /// <param name="urlPath">图片路径</param>
        /// <param name="thumUrlPath">缩略图</param>
        /// <param name="isimg">是否图片</param>
        /// <param name="truename">真实名称</param>
        /// <param name="contenttype">类型</param>
        /// <returns></returns>
        public AttachmentModel InsertAttachment(string urlPath, string thumUrlPath, bool isimg, string truename, string contenttype)
        {
            AttachmentModel model = new AttachmentModel();
            model.Id = Guid.NewGuid().ToString().ToUpper();
            model.UrlPath = urlPath;
            model.ThumUrlPath = thumUrlPath;
            model.IsImg = isimg;
            model.TrueName = truename;
            model.ContentType = contenttype;
            model.CreateDate = DateTime.Now;

            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var entity = _mapper.Map<AttachmentModel, SysAttachment>(model);
                db.SysAttachments.Add(entity);
                scope.SaveChanges();
            }
            return model;
        }

        public List<AttachmentModel> GetAttachmentsByObject(string objectId, string groupCode)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var query = db.SysObjectAttachments.Where(c => c.ObjectId == objectId);
                if (groupCode.IsNotBlank())
                {
                    query = query.Where(x => x.GroupCode == groupCode);
                }
                var attachmentIds = query.Select(c => c.AttachmentId).ToList();
                var attachmentModels = db.SysAttachments.Where(c => attachmentIds.Contains(c.Id))
                    .Select(c => new AttachmentModel()
                    {
                        Id = c.Id,
                        IsImg = (bool)c.IsImg,
                        UrlPath = c.UrlPath,
                        ThumUrlPath = c.ThumUrlPath,
                        TrueName = c.TrueName,
                        ContentType = c.ContentType,
                        CreateDate = c.CreateDate,
                    }).ToList();
                return attachmentModels;
            }
        }

        public bool Delete(string id)
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var data = db.SysAttachments.Include(x =>x.SysObjectAttachments).Where(x => x.Id ==id).ToList();
                db.SysAttachments.RemoveRange(data);
                return scope.SaveChanges() > 0;
            }
        }

        public List<AttachmentModel> GetNotUsedList()
        {
            using (var scope = _dbContextScopeFactory.Create())
            {
                var db = scope.DbContexts.Get<WikeDbContext>();
                var used = db.SysObjectAttachments.Select(x => x.AttachmentId);
               
                var attachmentModels = db.SysAttachments.Where(c => !used.Contains(c.Id)).OrderByDescending(x=>x.CreateDate)
                    .Select(c => new AttachmentModel()
                    {
                        Id = c.Id,
                        IsImg = (bool)c.IsImg,
                        UrlPath = c.UrlPath,
                        ThumUrlPath = c.ThumUrlPath,
                        TrueName = c.TrueName,
                        ContentType = c.ContentType,
                        CreateDate = c.CreateDate,
                    }).ToList();
                return attachmentModels;
            }
        }
    }
}
