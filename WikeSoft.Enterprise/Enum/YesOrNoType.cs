﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 是否枚举
    /// </summary>
    public enum YesOrNoType
    {
        [Description("否")]
        No = 0,

        [Description("是")]
        Yes = 1
    }
}
