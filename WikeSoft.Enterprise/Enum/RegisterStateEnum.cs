﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
   
    public enum RegisterStateEnum
    {
        /// <summary>
        /// 初始
        /// </summary>
        [Description("初始")]
        Init = 0,

        /// <summary>
        /// 转注
        /// </summary>
        [Description("转注")]
        Change = 1

        
    }
}
