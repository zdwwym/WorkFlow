﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
   
    public enum BCertificateTypeEnum
    {

        /// <summary>
        /// 带B
        /// </summary>
        [Description("带B")]
        HaveB = 0,

        /// <summary>
        /// 考B
        /// </summary>
        [Description("考B")]
        ExamB = 1,

        /// <summary>
        /// 考B
        /// </summary>
        [Description("不考B")]
        NoExamB = 2,

        /// <summary>
        /// 已换
        /// </summary>
        [Description("已换B证")]
        Changed = 3,

        /// <summary>
        /// 已转
        /// </summary>
        [Description("已转B证")]
        Turn = 4,
    }
}
