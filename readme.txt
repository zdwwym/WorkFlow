WorkFlow —— .NET 流程处理

WikeFlow官网：http://www.wikesoft.com

WikeFlow演示地址：http://workflow.wikesoft.com

WikeFlow2.0 演示地址：http://workflow2.wikesoft.com

WikeFlow2.0帮助文档：http://wikeflowhelp.wikesoft.com

WikeFlow2.0 主要功能
1、流程审批，一般的业务审批

2、流程会签，一个任务节点多人审批

3、流程驳回，流程不同意

4、流程转办，流程从一个人转交给另外的人处理

5、流程传阅，将流程传递给其他的人查阅

6、流程撤回，提交后可以撤回

7、流程附件，支持节点附件

8、流程审批中修改业务数据

9、流程审批中业务数据权限设置

多数据库支持
同时支持：SQLServer，Mysql，Oracle

 
