﻿using System.Web.Mvc;

namespace WikeSoft.Web
{
    public class CheckExplorerAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            //当WebUploader上传控件用Flash上传的时候， MajorVersion的值为0；
            if (filterContext.HttpContext.Request.Browser.MajorVersion !=0 && filterContext.HttpContext.Request.Browser.MajorVersion < 8)
            {
                ViewResult result = new ViewResult();
                result.ViewName = "ExplorerError";
                filterContext.Result = result;
            }
        }
    }
}