﻿using System.Web.Mvc;
using Star.Web;

namespace WikeSoft.Web
{
    /// <summary>
    /// Filter
    /// </summary>
    public class FilterConfig
    {
        /// <summary>
        /// 注册过滤器
        /// </summary>
        /// <param name="filters"></param>
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CheckExplorerAttribute());

            //filters.Add(new LisenceKeyFilter());

            //filters.Add(new AuthorizeAttribute());
            filters.Add(new WikeAuthorizeAttribute());
            filters.Add(new UnifyMvcSiteHandleErrorAttribute());
            filters.Add(new RightAttribute());
        }
    }
}
