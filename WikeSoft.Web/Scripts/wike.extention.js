﻿/*
** 此js是所有wike项目的js扩展，每个页面都必须调用
*/
(function () {
    //基础扩展Start
    //日期扩展
    Date.prototype.format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, //月份   
            "d+": this.getDate(), //日   
            "H+": this.getHours(), //小时   
            "m+": this.getMinutes(), //分   
            "s+": this.getSeconds(), //秒   
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度   
            "S": this.getMilliseconds() //毫秒   
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o) {
            if (o.hasOwnProperty(k)) {
                if (new RegExp("(" + k + ")").test(fmt))
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };
    //字符串扩展
    String.prototype.getDate = function () {
        var res = /\d{13}/.exec(this);
        var date = new Date(parseInt(res));
        return date.format("yyyy-MM-dd HH:mm:ss");
    };
    //字符串扩展
    String.prototype.getDateFormat = function (format) {
        var res = /\d{13}/.exec(this);
        var date = new Date(parseInt(res));
        return date.format(format);
    };
    //jq扩展
    $.fn.button = function(action) {
        var btn = $(this);
        var oldValue = btn.text();
        if (action === "reset") {
            btn.text(btn.attr("data-text"));
            btn.removeAttr("disabled");
            btn.removeClass("layui-btn-disabled");
        } else {
            btn.text(action);
            btn.attr("data-text", oldValue);
            btn.attr("disabled", "disabled");
            btn.addClass("layui-btn-disabled");
        }
    };
    //获取表单里所有的数据，转成json
    $.fn.toJson = function() {
        var o = {};
        var a = $(this).serializeArray();
        $.each(a,
            function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
            });
        return o;
    };
    //重置表单验证
    $.fn.resetFormValidate = function() {
        var form = $(this);
        form.removeData("validator");
        form.removeData("unobtrusiveValidation");
        $.validator.unobtrusive.parse(form);
    };

 
    //合并单元格
    //函数说明：合并指定表格（表格id为_w_table_id）指定列（行数大于_w_table_mincolnum 小于_w_table_maxcolnum）相同列中的相同文本的相邻单元格
    //          多于一列时，后一列的单元格合并范围不能超过前一列的合并范围。避免出现交错。
    //参数说明：_w_table_id 为需要进行合并单元格的表格id。如在HTMl中指定表格 id="data" ，此参数应为 #data
    //参数说明：_w_table_mincolnum 为需要进行比较合并的起始列数。为数字，则从最左边第一行为1开始算起。
    //          此参数可以为空，为空则第一列为起始列。
    //          需要注意，如果第一列为序号列，则需要排除此列。
    //参数说明：_w_table_maxcolnum 为需要进行比较合并的最大列数，列数大于这个数值的单元格将不进行比较合并。
    //          为数字，从最左边第一列为1开始算起。
    //          此参数可以为空，为空则同_w_table_mincolnum。
    $.fn.mergeCells = function (wTableMincolnum, wTableMaxcolnum) {
        var self = $(this);
        if (wTableMincolnum == void 0) { wTableMincolnum = 1; }
        if (wTableMaxcolnum == void 0) { wTableMaxcolnum = wTableMincolnum; }
        if (wTableMincolnum > wTableMaxcolnum) {
            return "";
        } else {
            var wTableSplitrow = new Array();
            for (var iLoop = wTableMincolnum; iLoop <= wTableMaxcolnum; iLoop++) {
                wTableOnerowspan(iLoop);
            }
        }
        function wTableOnerowspan(wTableColnum) {
            var _w_table_firstrow = 0; //前一列合并区块第一行
            _w_table_SpanNum = 0; //合并单元格时的，单元格Span个数
            _w_table_splitNum = 0; //数组的_w_table_splitrow的当前元素下标
            _w_table_Obj = self.find("tr td:nth-child(" + wTableColnum + ")");
            _w_table_curcol_rownum = _w_table_Obj.length - 1; //此列最后一行行数
            if (wTableSplitrow.length == 0) { wTableSplitrow[0] = _w_table_curcol_rownum; }
            _w_table_lastrow = wTableSplitrow[0]; //前一列合并区块最后一行
            var _w_table_firsttd;
            var _w_table_currenttd;
            var _w_table_curcol_splitrow = new Array();
            _w_table_Obj.each(function (i) {
                if (i == _w_table_firstrow) {
                    _w_table_firsttd = $(this);
                    _w_table_SpanNum = 1;
                } else {
                    _w_table_currenttd = $(this);
                    if (_w_table_firsttd.text() == _w_table_currenttd.text()) {
                        _w_table_SpanNum++;
                        _w_table_currenttd.hide(); //remove();
                        _w_table_firsttd.attr("rowSpan", _w_table_SpanNum);
                    } else {
                        _w_table_firsttd = $(this);
                        _w_table_SpanNum = 1;
                        setTableRow(i - 1);
                    }
                    if (i == _w_table_lastrow) { setTableRow(i); }
                }
                function setTableRow(_splitrownum) {
                    if (_w_table_lastrow <= _splitrownum && _w_table_splitNum++ < wTableSplitrow.length) {
                        //_w_table_firstrow=_w_table_lastrow+1;
                        _w_table_lastrow = wTableSplitrow[_w_table_splitNum];
                    }
                    _w_table_curcol_splitrow[_w_table_curcol_splitrow.length] = _splitrownum;
                    if (i < _w_table_curcol_rownum) { _w_table_firstrow = _splitrownum + 1; }
                }
            });
            wTableSplitrow = _w_table_curcol_splitrow;
        }
    };
	
})();


//得到url地址的参数 
var getQueryString = function (name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
};

function isNumber(s) {
    var regu = "^[0-9]+\.?[0-9]*$";
    //    var regu = "^[0-9]*$";
    var re = new RegExp(regu);
    if (re.test(s)) {
        return true;
    }
    else {
        return false;
    }
}