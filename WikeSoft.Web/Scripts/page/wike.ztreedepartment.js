﻿ 


var menuSetting = {
    async: {
        enable: true,
        url: "/department/GetMyDepartmentList",
        autoParam: ["id", "name=n", "level=lv"],
        otherParam: { "otherParam": "tree" }
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    check: {
        enable: true,
        chkboxType: { "Y": "ps", "N": "ps" }
    }
};

var setting = {
    async: {
        enable: true,
        url: "/department/GetMyDepartmentList",
        autoParam: ["id", "name=n", "level=lv"],
        otherParam: { "otherParam": "tree" }
    },
    check: {
        enable: true,
        chkboxType: { "Y": "", "N": "" }
    },
    view: {
        dblClickExpand: false
    },
    data: {
        simpleData: {
            enable: true
        }
    },
    callback: {
        beforeClick: beforeClick,
        onCheck: onCheck
    }
};
function beforeClick(treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo");
    zTree.checkNode(treeNode, !treeNode.checked, null, true);
    return false;
}

function onCheck(e, treeId, treeNode) {
    var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
    nodes = zTree.getCheckedNodes(true),
    v = "";
    var ids = []; 
    for (var i = 0, l = nodes.length; i < l; i++) {
        v += nodes[i].name + ",";
        ids.push(nodes[i].id);
    }
    if (v.length > 0) v = v.substring(0, v.length - 1);
    var cityObj = $("#Department");
    cityObj.attr("value", v);

    setUserByDepartments(ids);
}

function setUserByDepartments(ids)
{
    $.ajax({
        url: '/User/GetUsersByDepartment',
        type: "POST",
        dataType: "json",
        data: JSON.stringify(ids),
        contentType: "application/json, charset=utf-8",
        success: function (users) {
            var options = "<option value=''></option>";
            for (var i = 0; i < users.length; i++) {
                var user = users[i];
                options += "<option value='"+user.Id+"'>"+user.TrueName+"</option>";
            }
            $("#UserId").html(options);
        }
    });
}

function showMenu() {
    var cityObj = $("#Department");
    var cityOffset = $("#Department").offset();
    $("#menuContent").css({ left: cityOffset.left + "px", top: cityOffset.top + cityObj.outerHeight() + "px" }).slideDown("fast");

    $("body").bind("mousedown", onBodyDown);
}
function hideMenu() {
    $("#menuContent").fadeOut("fast");
    $("body").unbind("mousedown", onBodyDown);
}
function onBodyDown(event) {
    if (!(event.target.id == "menuBtn" || event.target.id == "Department" || event.target.id == "menuContent" || $(event.target).parents("#menuContent").length > 0)) {
        hideMenu();
    }
}


$(document).ready(function () {
 
    $.fn.zTree.init($("#treeDemo"), setting);

    $("#btnSpecialSearch").click(function () {
       
        var departments = [];
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        var nodes = zTree.getCheckedNodes(true);
  
        
        for (var i = 0, l = nodes.length; i < l; i++) {
            departments.push(nodes[i].id);
        }


        var postJson = $("#searchToolBar").toJson();
      
        postJson.Departments = departments;
       // var parms = JSON.stringify(postJson);
        WikePage.Search(postJson);
        return false;
    });
  
});