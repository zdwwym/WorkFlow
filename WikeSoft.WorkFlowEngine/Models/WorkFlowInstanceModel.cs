﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Enum;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class WorkFlowInstanceModel
    {

        ///<summary>
        /// 流程实例Id
        ///</summary>
        public string Id { get; set; } // Id (Primary key) (length: 50)

        ///<summary>
        /// 流程定义ID
        ///</summary>
        public string FlowDefId { get; set; } // FlowDefId (length: 50)

        ///<summary>
        /// 创建人
        ///</summary>
        public string CreateUser { get; set; } // CreateUser (length: 50)

        ///<summary>
        /// 创建人用户Id
        ///</summary>
        public string CreateUserId { get; set; } // CreateUserId (length: 50)

        ///<summary>
        /// CreateTime
        ///</summary>
        public System.DateTime CreateTime { get; set; } // CreateTime

        ///<summary>
        /// 流程状态(0：历史，1：运行)
        ///</summary>
        public FlowRunStatus RunStatus { get; set; } // RunStatus

        ///<summary>
        /// 版本
        ///</summary>
        public int Version { get; set; } // Version

        ///<summary>
        /// 业务Id
        ///</summary>
        public string ObjectId { get; set; } // ObjectId (length: 100)

        ///<summary>
        /// 实例名称
        ///</summary>
        public string InstanceName { get; set; } // InstanceName (length: 100)

        ///<summary>
        /// 关联用户
        ///</summary>
        public string AssociatedUserId { get; set; } // AssociatedUserId (length: 2000)

        ///<summary>
        /// 业务路径
        ///</summary>
        public string BusinessUrl { get; set; } // BusinessUrl (length: 255)

        ///<summary>
        /// 审核页面
        ///</summary>
        public string AuditUrl { get; set; } // AuditUrl (length: 255)

        public string OtherParams { get; set; }

        public string BusinessPath
        {
            get
            {
                if (!string.IsNullOrEmpty(this.OtherParams))
                {
                    return this.BusinessUrl + "?objectId=" + ObjectId + "&instanceId=" + Id+"&"+OtherParams;
                }
                
                return this.BusinessUrl + "?objectId=" + ObjectId+"&instanceId="+ Id;
            }
        }

        public string CategoryId { get; set; }
        public string CategoryName { get; set; }

    }
}
