
INSERT INTO WORKFLOW."WorkFlow_Category" ("Id", "CategoryName", "SortIndx", "ParentId", "PathCode") VALUES ('2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', '所有', '1', NULL, 'AA');
INSERT INTO WORKFLOW."WorkFlow_Category"("Id", "CategoryName", "SortIndx", "ParentId", "PathCode") VALUES ('547FEDC8-DFA0-475C-BF64-7EE43ED41E35', '请假申请', '1', '2ADE9FDE-FFD8-4536-8E81-AA1BC8C7B2BD', 'AAAA');


INSERT INTO WORKFLOW."WorkFlow_Def"("Id", "WorkFlowDefKey", "WorkFlowDefName", "CreateDate", "IsDelete", "BusinessUrl", "AuditUrl", "CategoryId") VALUES ('96F5B45A-6C94-4058-A76C-879FE6FFC09B', 'holiday', '请假申请', TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'), '0', '#', '#', '547FEDC8-DFA0-475C-BF64-7EE43ED41E35');

INSERT INTO WORKFLOW."WorkFlow_Version" ("Id", "WorkFlowDefId", "WorkFlowDocument", "VersionNumber", "CreateDate", "Remark") VALUES ('9E3E214A-EFEF-41F5-B627-01AA83380B0D', '96F5B45A-6C94-4058-A76C-879FE6FFC09B', '', '1', TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'), NULL);




--------------------------------------------------------
--  文件已创建 - 星期三-五月-09-2018   
--------------------------------------------------------

Insert into WORKFLOW."Sys_Department" ("Id","DepartmentName","DepartmentCode","ParentId","Remark","PathCode","IsDelete") values ('20CE56AA-0645-41FD-9FF6-51364D0A62F7','行政部',null,'884FC7B8-47A1-44FD-8970-4FF16A655E87',null,'AAAAAB',0);
Insert into WORKFLOW."Sys_Department" ("Id","DepartmentName","DepartmentCode","ParentId","Remark","PathCode","IsDelete") values ('4A7335F8-367E-4C06-82C0-4DB694DEC5E2','Wikesoft',null,'00000000-0000-0000-0000-000000000000',null,'AA',0);
Insert into WORKFLOW."Sys_Department" ("Id","DepartmentName","DepartmentCode","ParentId","Remark","PathCode","IsDelete") values ('59485231-7B38-4A95-BCDA-721F87A5BFBB','研发部',null,'884FC7B8-47A1-44FD-8970-4FF16A655E87',null,'AAAAAA',0);
Insert into WORKFLOW."Sys_Department" ("Id","DepartmentName","DepartmentCode","ParentId","Remark","PathCode","IsDelete") values ('884FC7B8-47A1-44FD-8970-4FF16A655E87','总经理办公室',null,'4A7335F8-367E-4C06-82C0-4DB694DEC5E2',null,'AAAA',0);
Insert into WORKFLOW."Sys_Department" ("Id","DepartmentName","DepartmentCode","ParentId","Remark","PathCode","IsDelete") values ('B0DF6F62-7068-4FA6-82B2-597494327937','研发一部',null,'59485231-7B38-4A95-BCDA-721F87A5BFBB',null,'AAAAAAAA',0);
 
Insert into WORKFLOW."Sys_KeyValue" ("Id","CnName","Key","Value","Memo") values ('46AEFF2D-5DD4-4470-97B6-64757042EC59','系统名称','SystemTitle','WikeFlow','系统名称');
 
 
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('21BFCC9C-CCD1-4CD4-A190-053137F234A3','3D794E41-E1EE-4315-AAF1-4A7732169416','100.06','流程类别管理','/WorkFlowCategory/Index',null,1,null,'AAAF',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('2A094905-4B7D-42D8-945F-FC6CE34E8A99','3D794E41-E1EE-4315-AAF1-4A7732169416','100.03','功能管理','/Page/Index',null,1,null,'AAAC',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('3D794E41-E1EE-4315-AAF1-4A7732169416',null,'100','系统设置','#',null,1,'fa-cog','AA',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('4BC3B535-E1CB-4A3E-B1E0-89B7AF981796','3D794E41-E1EE-4315-AAF1-4A7732169416','100.01','角色管理','/Role/Index',null,1,null,'AAAA',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('5B39998B-7322-48E3-94F8-AFC16FB79379','D2429C03-0692-4DB6-A499-DD827F2A9915','200.99','与我相关','/WorkFlowCategory/HistoryListView',null,1,null,'ABAC',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F','D2429C03-0692-4DB6-A499-DD827F2A9915','200.02','请假审核','/Holiday/Audit',null,1,null,'ABAB',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('717EF415-189B-4CDD-A2B1-6DE8826D8493','3D794E41-E1EE-4315-AAF1-4A7732169416','100.04','功能授权','/Role/SetRights',null,1,null,'AAAD',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('74B762A7-30B9-482F-B164-B658A4BA2271','3D794E41-E1EE-4315-AAF1-4A7732169416','100.08','运行参数设置','/KeyValue/Index',null,1,null,'AAAH',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('929D6813-1856-445E-A612-E804812877FC','3D794E41-E1EE-4315-AAF1-4A7732169416','100.05','用户管理','/User/Index',null,1,null,'AAAE',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('BA3AF546-24E0-4725-A253-E58595E18447','D2429C03-0692-4DB6-A499-DD827F2A9915','200.01','请假申请','/Holiday/Index',null,1,null,'ABAA',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('C4DCE65B-44A2-4384-AE08-5D80A687A1CC','3D794E41-E1EE-4315-AAF1-4A7732169416','100.02','部门管理','/Department/Index',null,1,null,'AAAB',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('D2429C03-0692-4DB6-A499-DD827F2A9915',null,'200','流程管理','#',null,1,'fa-commenting','AB',0);
Insert into WORKFLOW."Sys_Page" ("Id","ParentId","PageCode","PageName","PagePath","Description","IsUsed","CssName","PathCode","IsDelete") values ('F9A43C22-EBEA-475D-BDE7-81CDCFAA7352','3D794E41-E1EE-4315-AAF1-4A7732169416','100.07','流程设计','/FlowDesign/Index',null,1,null,'AAAG',0);
 
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','系统管理员','系统管理员',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('1230C652-B69B-4407-95FC-EBB7EB964265','项目经理','项目经理',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','行政','行政',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','部门主管','部门主管',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('89ABE7D7-75A0-47D6-BA01-333839222119','总经理','总经理',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('A5208032-3C5B-4CDC-91E4-33331EFB7A55','流程设计','流程设计',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
Insert into WORKFLOW."Sys_Role" ("Id","RoleName","Remark","CreateDate","IsDelete") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','普通员工','普通员工',TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),0);
 
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','21BFCC9C-CCD1-4CD4-A190-053137F234A3');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','2A094905-4B7D-42D8-945F-FC6CE34E8A99');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','4BC3B535-E1CB-4A3E-B1E0-89B7AF981796');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','717EF415-189B-4CDD-A2B1-6DE8826D8493');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','74B762A7-30B9-482F-B164-B658A4BA2271');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','929D6813-1856-445E-A612-E804812877FC');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','C4DCE65B-44A2-4384-AE08-5D80A687A1CC');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('067A9FFD-462F-4398-8006-2B5657CBDDE6','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('1230C652-B69B-4407-95FC-EBB7EB964265','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('1230C652-B69B-4407-95FC-EBB7EB964265','5B39998B-7322-48E3-94F8-AFC16FB79379');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('1230C652-B69B-4407-95FC-EBB7EB964265','66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('1230C652-B69B-4407-95FC-EBB7EB964265','D2429C03-0692-4DB6-A499-DD827F2A9915');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('1230C652-B69B-4407-95FC-EBB7EB964265','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','5B39998B-7322-48E3-94F8-AFC16FB79379');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','D2429C03-0692-4DB6-A499-DD827F2A9915');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('4997DC7A-BE5F-4256-AEF6-6CB321C73243','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','5B39998B-7322-48E3-94F8-AFC16FB79379');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','D2429C03-0692-4DB6-A499-DD827F2A9915');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('61D72D73-ACD4-4EF9-82BA-DD231B7253BC','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('89ABE7D7-75A0-47D6-BA01-333839222119','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('89ABE7D7-75A0-47D6-BA01-333839222119','5B39998B-7322-48E3-94F8-AFC16FB79379');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('89ABE7D7-75A0-47D6-BA01-333839222119','66CF5B3B-EF30-4C3E-AE1C-9C8E2FC1658F');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('89ABE7D7-75A0-47D6-BA01-333839222119','D2429C03-0692-4DB6-A499-DD827F2A9915');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('89ABE7D7-75A0-47D6-BA01-333839222119','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('A5208032-3C5B-4CDC-91E4-33331EFB7A55','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('A5208032-3C5B-4CDC-91E4-33331EFB7A55','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','3D794E41-E1EE-4315-AAF1-4A7732169416');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','5B39998B-7322-48E3-94F8-AFC16FB79379');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','BA3AF546-24E0-4725-A253-E58595E18447');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','D2429C03-0692-4DB6-A499-DD827F2A9915');
Insert into WORKFLOW."Sys_RolePage" ("RoleId","PageId") values ('E8DC5DBF-C753-4D08-BE50-B4E017209E74','F9A43C22-EBEA-475D-BDE7-81CDCFAA7352');
 
Insert into WORKFLOW."Sys_User" ("Id","UserName","UserCode","PassWord","TrueName","Email","Mobile","UserStatus","PasswordExpirationDate","DepartmentId","IsDelete") values ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4','一部员工',null,'c4ca4238a0b923820dcc509a6f75849b','一部员工',null,null,1,TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),'B0DF6F62-7068-4FA6-82B2-597494327937',0);
Insert into WORKFLOW."Sys_User" ("Id","UserName","UserCode","PassWord","TrueName","Email","Mobile","UserStatus","PasswordExpirationDate","DepartmentId","IsDelete") values ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46','研发部门主管',null,'c4ca4238a0b923820dcc509a6f75849b','研发部门主管',null,null,1,TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),'59485231-7B38-4A95-BCDA-721F87A5BFBB',0);
Insert into WORKFLOW."Sys_User" ("Id","UserName","UserCode","PassWord","TrueName","Email","Mobile","UserStatus","PasswordExpirationDate","DepartmentId","IsDelete") values ('A22C56A2-78D7-4B61-8609-D3AD98876159','总经理',null,'c4ca4238a0b923820dcc509a6f75849b','总经理',null,null,1,TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),'884FC7B8-47A1-44FD-8970-4FF16A655E87',0);
Insert into WORKFLOW."Sys_User" ("Id","UserName","UserCode","PassWord","TrueName","Email","Mobile","UserStatus","PasswordExpirationDate","DepartmentId","IsDelete") values ('C9028C6D-1AEB-473C-8105-4CF03C59766B','一部项目经理',null,'c4ca4238a0b923820dcc509a6f75849b','一部项目经理',null,null,1,TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),'B0DF6F62-7068-4FA6-82B2-597494327937',0);
Insert into WORKFLOW."Sys_User" ("Id","UserName","UserCode","PassWord","TrueName","Email","Mobile","UserStatus","PasswordExpirationDate","DepartmentId","IsDelete") values ('D34BE365-8F07-4FEE-9713-B079EC4AF9A3','admin',null,'c4ca4238a0b923820dcc509a6f75849b','系统管理员',null,null,1,TO_DATE('2018-05-09 09:58:20', 'SYYYY-MM-DD HH24:MI:SS'),'4A7335F8-367E-4C06-82C0-4DB694DEC5E2',0);
 
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4','A5208032-3C5B-4CDC-91E4-33331EFB7A55');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('64FAEF75-C62E-4EAE-89E4-D4DDE26CD4D4','E8DC5DBF-C753-4D08-BE50-B4E017209E74');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46','61D72D73-ACD4-4EF9-82BA-DD231B7253BC');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('97DFDEE3-FA0D-470F-92C0-3FD0EDC5AB46','A5208032-3C5B-4CDC-91E4-33331EFB7A55');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('A22C56A2-78D7-4B61-8609-D3AD98876159','89ABE7D7-75A0-47D6-BA01-333839222119');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('A22C56A2-78D7-4B61-8609-D3AD98876159','A5208032-3C5B-4CDC-91E4-33331EFB7A55');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('C9028C6D-1AEB-473C-8105-4CF03C59766B','1230C652-B69B-4407-95FC-EBB7EB964265');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('C9028C6D-1AEB-473C-8105-4CF03C59766B','A5208032-3C5B-4CDC-91E4-33331EFB7A55');
Insert into WORKFLOW."Sys_UserRole" ("UserId","RoleId") values ('D34BE365-8F07-4FEE-9713-B079EC4AF9A3','067A9FFD-462F-4398-8006-2B5657CBDDE6');



DECLARE  
  clobValue WORKFLOW."WorkFlow_Version"."WorkFlowDocument"%TYPE; 
BEGIN  
  clobValue := '<?xml version="1.0"?>
<WorkFlowDocument xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
  <Nodes>
    <TaskDefinition>
      <Id>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>开始</NodeDefName>
      <LeftX>71</LeftX>
      <TopX>47</TopX>
      <Width>54</Width>
      <Height>31</Height>
      <NodeType>start</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>A5B8BC9F-4391-4540-0274-727C005FEFBB</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>项目经理审核</NodeDefName>
      <LeftX>199</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>项目经理;</RoleNames>
      <RoleIds>1230C652-B69B-4407-95FC-EBB7EB964265;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>结束</NodeDefName>
      <LeftX>678</LeftX>
      <TopX>47</TopX>
      <Width>81</Width>
      <Height>31</Height>
      <NodeType>end</NodeType>
    </TaskDefinition>
    <TaskDefinition>
      <Id>250AD77D-0E34-494B-0BFD-0350966A0097</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>部门主管审核</NodeDefName>
      <LeftX>363</LeftX>
      <TopX>47</TopX>
      <Width>83</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>部门主管;</RoleNames>
      <RoleIds>61D72D73-ACD4-4EF9-82BA-DD231B7253BC;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>行政备案</NodeDefName>
      <LeftX>534</LeftX>
      <TopX>47</TopX>
      <Width>79</Width>
      <Height>31</Height>
      <NodeType>task</NodeType>
      <RoleNames>行政;</RoleNames>
      <RoleIds>4997DC7A-BE5F-4256-AEF6-6CB321C73243;</RoleIds>
    </TaskDefinition>
    <TaskDefinition>
      <Id>527F46EA-6DE4-42B8-04AB-63B0C905C14D</Id>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <NodeDefName>总经理审核</NodeDefName>
      <LeftX>360</LeftX>
      <TopX>166</TopX>
      <Width>89</Width>
      <Height>32</Height>
      <NodeType>task</NodeType>
      <RoleNames>总经理;</RoleNames>
      <RoleIds>89ABE7D7-75A0-47D6-BA01-333839222119;</RoleIds>
    </TaskDefinition>
  </Nodes>
  <NodeLinks>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>AFE7D363-5886-46E1-0D4E-B305EF7F85CA</SourceId>
      <TargetId>A5B8BC9F-4391-4540-0274-727C005FEFBB</TargetId>
      <LinkId>con_49</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>124</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>196</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 36.5 0 M 35.5 0 L 41.5 0 M 41 0 L 71 0 </Path>
      <LinkNameLeft>163</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>A5B8BC9F-4391-4540-0274-727C005FEFBB</SourceId>
      <TargetId>250AD77D-0E34-494B-0BFD-0350966A0097</TargetId>
      <LinkId>con_51</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>277</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>360</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 42 0 M 41 0 L 52.5 0 M 52 0 L 82 0 </Path>
      <LinkNameLeft>321</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_53</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>445</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>531</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 43.5 0 M 42.5 0 L 55.5 0 M 55 0 L 85 0 </Path>
      <LinkName>小于3天</LinkName>
      <Condition>1</Condition>
      <LinkNameLeft>491</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>250AD77D-0E34-494B-0BFD-0350966A0097</SourceId>
      <TargetId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</TargetId>
      <LinkId>con_55</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>403</StartLeft>
      <StartTop>75</StartTop>
      <StartPostion>BottomCenter</StartPostion>
      <EndLeft>403</EndLeft>
      <EndTop>163</EndTop>
      <EndPostion>TopCenter</EndPostion>
      <Path>M 0 0.5 L 0 31.5 M 0 30.5 L 0 44.5 M 0 43.5 L 0 57.5 M 0 57 L 0 87 </Path>
      <LinkName>大于等于3天</LinkName>
      <Condition>2</Condition>
      <LinkNameLeft>405</LinkNameLeft>
      <LinkNameTop>122</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>527F46EA-6DE4-42B8-04AB-63B0C905C14D</SourceId>
      <TargetId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</TargetId>
      <LinkId>con_57</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>448</StartLeft>
      <StartTop>179</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>572</EndLeft>
      <EndTop>75</EndTop>
      <EndPostion>BottomCenter</EndPostion>
      <Path>M 0.5 104 L 31.5 104 M 30.5 104 L 124 104 M 123.5 104.5 L 123.5 30.5 M 123.5 31 L 123.5 1 </Path>
      <LinkNameLeft>564</LinkNameLeft>
      <LinkNameTop>182</LinkNameTop>
    </TaskLink>
    <TaskLink>
      <FlowDefId>96F5B45A-6C94-4058-A76C-879FE6FFC09B</FlowDefId>
      <SourceId>4539DB86-DB0B-4633-0BAA-9DC61DFB84B2</SourceId>
      <TargetId>29671CA8-F791-45F3-01C4-7BDA2EE8C9CA</TargetId>
      <LinkId>con_59</LinkId>
      <LinkType>Flowchart</LinkType>
      <StartLeft>612</StartLeft>
      <StartTop>60</StartTop>
      <StartPostion>RightMiddle</StartPostion>
      <EndLeft>675</EndLeft>
      <EndTop>60</EndTop>
      <EndPostion>LeftMiddle</EndPostion>
      <Path>M 0.5 0 L 31.5 0 M 30.5 0 L 32 0 M 31 0 L 32.5 0 M 32 0 L 62 0 </Path>
      <LinkNameLeft>646</LinkNameLeft>
      <LinkNameTop>62</LinkNameTop>
    </TaskLink>
  </NodeLinks>
</WorkFlowDocument>'; --字段内容  
  UPDATE WORKFLOW."WorkFlow_Version" SET "WorkFlowDocument" = clobValue WHERE "Id"='9E3E214A-EFEF-41F5-B627-01AA83380B0D';  
  COMMIT;  
END; 



